# mcli: Morello command line interface

## Install docker
```
$ curl -sSL https://get.docker.com | sh
```
**Note:** This guide requires to run docker as a non-root user. After the completion of the command above execute:
```
$ dockerd-rootless-setuptool.sh install
```
and follow the instructions on screen. For further information please refer to the [Docker](https://docs.docker.com/) documentation.
## Install docker-compose
Latest: v2.25.0
Installation command:
```
$ sudo curl -L "https://github.com/docker/compose/releases/download/v2.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Provide correct permissions to docker compose:
```
$ sudo chmod +x /usr/local/bin/docker-compose
```
Test docker-compose:
```
$ docker-compose --version
```
## mcli
Create and enter a python virtualenv (*optional*):
```
python -m venv venv
. venv/bin/activate
```
Install mcli:
```
pip install git+https://git.morello-project.org/morello/mcli.git
```
Create a new workspace:
```
mcli new <workspace_name>
```
```
Project Name: <workspace_name>
✔ Complete!
```
Enter the workspace:
```
cd <workspace_name>
```
Setup dependencies:
```
mcli setup
```
Bootstrap the environment:
```
mcli up
```
Connect to environment (linux/sdk):
```
mcli connect
```
```
? Select instance:  linux
✔ Connect to: workspace-morello-linux
root@b3077bf44e7f:/morello# exit
exit
✔ Complete!
```
