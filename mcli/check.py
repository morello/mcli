import typer
import subprocess
import os
import requests
import tarfile
from InquirerPy import prompt
from rich import print

def execute_command(command: str):
    result = subprocess.run(command, shell=True, capture_output=True, text=True)

    return result.stdout

def docker_compose_version():
    command = "docker-compose --version"

    result = execute_command(command)

    if "Docker Compose version" in result:
        return True
    else:
        print(":cross_mark: [red bold]Docker Compose not found, please install docker before proceeding![/red bold]")
        return False

def docker_version():
    command = "docker --version"

    if docker_compose_version() is True:
        result = execute_command(command)

        if "Docker version" in result:
            return True
        else:
            print(":cross_mark: [red bold]Docker not found, please install docker before proceeding![/red bold]")
            return False
    else:
        return False
