import typer
import subprocess
from typing import Optional
from typing_extensions import Annotated
from InquirerPy import prompt
from rich import print
from . import project
from . import check

app = typer.Typer()

@app.command("new")
def new(project_name: str):
    """
    Creates a new workspace with project_name
    """
    if check.docker_version() is True:
        project.create(project_name)

@app.command("setup")
def setup():
    """
    Sets up all the dependencies
    """
    if check.docker_version() is True:
        project.setup()

@app.command("up")
def up():
    """
    Initializes the docker containers
    """
    if check.docker_version() is True:
        project.up()

@app.command("update")
def up():
    """
    Updates the docker containers
    """
    if check.docker_version() is True:
        project.update()

@app.command("connect")
def connect(instance: Annotated[Optional[str], typer.Argument()] = None):
    """
    Connects either to the sdk or to the linux container
    """
    if check.docker_version() is True:
        project.connect(instance)

if __name__ == '__main__':
    app()

