import typer
import subprocess
import os
import requests
import tarfile
from InquirerPy import prompt
from rich import print

def write_dot_project(project_name: str):
    f = open(f"{project_name}/.project", "w")
    f.write(f"{project_name}\n")
    f.close()

def read_dot_project():
    f = open(".project", "r")
    p = f.read()
    f.close()

    return p.strip()

def docker_compose(project_name: str):
    template = [
        "# Docker composer file for Morello Linux \n",
        "version: \'3.8\' \n",
        "services: \n",
        "  <project>-morello-sdk: \n",
        "    image: \"git.morello-project.org:5050/morello/morello-sdk/morello-sdk:latest\" \n",
        "    container_name: \"<project>-morello-sdk\" \n",
        "    volumes: \n"
        "      - ./shared_folder:/home/morello/shared_folder \n",
        "      - ./workspace:/home/morello/workspace \n",
        "    tty: true \n",
        "    restart: unless-stopped \n",
        "\n",
        "  <project>-morello-linux: \n"
        "    image: \"git.morello-project.org:5050/morello/morello-linux/morello-linux:latest\" \n",
        "    container_name: \"<project>-morello-linux\" \n",
        "    environment: \n",
        "      - UID=1000 \n",
        "      - GID=1000 \n",
        "    volumes: \n",
        "      - ./morello:/morello \n",
        "      - ./shared_folder:/morello/shared_folder \n",
        "    tty: true \n",
        "    restart: unless-stopped \n",
    ]

    compose = []

    for t in template:
        compose += t.replace("<project>", f"{project_name}")

    f = open(f"{project_name}/docker-compose.yml", "w")
    f.writelines(compose)
    f.close()

def setup():
    model = "FVP_Morello_0.11_34.tgz"
    url = "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Morello%20Platform/FVP_Morello_0.11_34.tgz?rev=5f34837ae6c14ede8493dfc24c9af397&hash=862883120C5638E0B3C5ACA6FDDC5558021E1886"

    current = os.getcwd()
    morello = f"{current}/morello"

    os.chdir(morello)

    response = requests.get(url)
    open(model, "wb").write(response.content)

    tar = tarfile.open(model)
    tar.extractall()
    tar.close()

    command = f"./FVP_Morello.sh --force --destination ./FVP_Morello".strip()

    subprocess.run(command, shell=True)

    print(":heavy_check_mark: [green bold]Complete![/green bold]")

def create(project_name: str):
    print(f"Project Name: [blue bold]{project_name}[/blue bold]")

    # Create the project directory structure
    subprocess.run(f"mkdir -p {project_name}", shell=True)
    subprocess.run(f"mkdir -p {project_name}/morello", shell=True)
    subprocess.run(f"mkdir -p {project_name}/shared_folder", shell=True)
    subprocess.run(f"mkdir -p {project_name}/workspace", shell=True)

    # Create Docker Compose
    docker_compose(project_name)

    # Add .project file
    write_dot_project(f"{project_name}")

    print(":heavy_check_mark: [green bold]Complete![/green bold]")

def up():
    # Spawn the docker images
    subprocess.run("docker compose up -d", shell=True)

    print(":heavy_check_mark: [green bold]Complete![/green bold]")

def update():
    # Update docker images
    subprocess.run("docker compose pull", shell=True)
    up()

    print(":heavy_check_mark: [green bold]Complete![/green bold]")

def connect(instance: str):
    if instance is None:
        instances = [
            {
                'type': 'list',
                'name': 'instance',
                'message': 'Select instance: ',
                'choices': ['linux', 'sdk' ],
            }
        ]

        instance_hash = prompt(instances)
        instance = instance_hash['instance']

    project_name = read_dot_project()

    container = f"{project_name}-morello-{instance}"

    print(f":heavy_check_mark: [green bold]Connect to: [/green bold][blue bold]{container}[/blue bold]")

    subprocess.run(f"docker exec -it -u root {container} /bin/bash", shell=True)

    print(":heavy_check_mark: [green bold]Complete![/green bold]")
